﻿using System;
using System.Threading.Tasks;

namespace AnkiNet.Model
{
    public static class AnkiClientModelExtensions
    {
        public static async Task<CommandResult<string[]?>> ModelNamesAsync(this AnkiClient client)
        {
            var methodInfo = new Func<AnkiClient, Task<CommandResult<string[]?>>>(ModelNamesAsync).Method;
            return await client.Resolve<string[]?>(methodInfo);
        }

        public static async Task<CommandResult<string[]?>> ModelFieldNamesAsync(this AnkiClient client,
            string modelName)
        {
            var methodInfo = new Func<AnkiClient, string, Task<CommandResult<string[]?>>>(ModelFieldNamesAsync).Method;
            return await client.Resolve<string[]?>(methodInfo, modelName);
        }
    }
}