﻿using System;

namespace AnkiNet
{
    public class AnkiClientConfiguration
    {
        /// <summary>
        ///     Anki connect server port.
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        ///     Anki connect server ip.
        /// </summary>
        public string Ip { get; set; } = "";
        /// <summary>
        ///     Anki connect server response timeout in milliseconds.
        /// </summary>
        public TimeSpan ResponseTimeout { get; set; }
        public static AnkiClientConfiguration Default => new()
        {
            Port = 8765,
            Ip = "http://127.0.0.1",
            ResponseTimeout = new TimeSpan(0, 0, 4),
        };

        private AnkiClientConfiguration()
        {
        }

        public AnkiClientConfiguration(int port, string ip, TimeSpan responseTimeout)
        {
            Port = port;
            Ip = ip;
            ResponseTimeout = responseTimeout;
        }
    }
}