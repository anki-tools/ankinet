﻿namespace AnkiNet
{
    public class CommandResult<T>
    {
        public T? Result { get; set; }
        public string? Error { get; set; }

        public CommandResult()
        {
        }

        public CommandResult(T? result, string? error)
        {
            Result = result;
            Error = error;
        }
    }
}