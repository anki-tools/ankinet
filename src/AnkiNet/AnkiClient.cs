﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;

namespace AnkiNet
{
    public class AnkiClient
    {
        private readonly HttpClient _client;
        private readonly Uri _uri;

        public AnkiClient() : this(AnkiClientConfiguration.Default)
        {
        }

        public AnkiClient(AnkiClientConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _client = new HttpClient(new SocketsHttpHandler
            {
                ConnectTimeout = configuration.ResponseTimeout,
            });
            _uri = new Uri($"{configuration.Ip}:{configuration.Port}");
        }

        internal async Task<CommandResult<T>> Resolve<T>(MethodBase methodBase, params object[] arguments)
        {
            var actionDictionary = new Dictionary<string, object>
            {
                {"action", char.ToLower(methodBase.Name[0]) + methodBase.Name[1..].Replace("Async", "")},
                {"version", 6},
            };
            var paramsDictionary = new Dictionary<string, object>();
            var parameters = methodBase.GetParameters();
            var extensionMethodFlag = parameters.Any(p => p.ParameterType == typeof(AnkiClient));
            foreach (var p in parameters)
                if (p.ParameterType != typeof(AnkiClient))
                    paramsDictionary.Add(p.Name ?? throw new MissingMethodException(nameof(methodBase)),
                        arguments[extensionMethodFlag ? p.Position - 1 : p.Position]);
            actionDictionary.Add("params", paramsDictionary);

            Stream? request = new MemoryStream(), response = null;
            try
            {
                await JsonSerializer.SerializeAsync(request, actionDictionary);
                request.Position = 0;
                response = await (await _client.PostAsync(_uri, new StreamContent(request))).Content
                    .ReadAsStreamAsync();
                return await JsonSerializer.DeserializeAsync<CommandResult<T>>(response, new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                }) ?? new CommandResult<T>(default, null);
            }
            catch (Exception e)
            {
                request.Position = 0;
                if (response != null)
                    response.Position = 0;
                throw new AnkiConnectException(e, request, response);
            }
            finally
            {
                await request.DisposeAsync();
                if (response != null)
                    await response.DisposeAsync();
            }
        }
    }
}