﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AnkiNet.Notes
{
    public class Note
    {
        [JsonPropertyName("deckName")]
        public string? DeckName { get; set; }
        [JsonPropertyName("modelName")]
        public string? ModelName { get; set; }
        [JsonPropertyName("fields")]
        public Dictionary<string, string>? Fields { get; set; }
        [JsonPropertyName("tags")]
        public string[]? Tags { get; set; }
    }
}