﻿using System.Collections.Generic;

namespace AnkiNet.Notes
{
    public class NoteBuilder
    {
        private string? _modelName = null;
        private string? _deckName = null;
        private List<string> _tags = new();
        private Dictionary<string, string> _fields = new();

        public NoteBuilder()
        {
        }

        public NoteBuilder SetModelName(string modelName)
        {
            _modelName = modelName;
            return this;
        }

        public NoteBuilder SetDeckName(string deckName)
        {
            _deckName = deckName;
            return this;
        }

        public NoteBuilder AddTag(string tag)
        {
            _tags.Add(tag);
            return this;
        }

        public NoteBuilder AddField(string key, string value)
        {
            _fields.Add(key, value);
            return this;
        }

        public Note Build()
        {
            return new Note
            {
                ModelName = _modelName,
                DeckName = _deckName,
                Tags = _tags.ToArray(),
                Fields = _fields,
            };
        }
    }
}