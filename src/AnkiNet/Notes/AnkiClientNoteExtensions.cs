﻿using System;
using System.Threading.Tasks;

namespace AnkiNet.Notes
{
    public static class AnkiClientNoteExtensions
    {
        public static async Task<CommandResult<long>> AddNoteAsync(this AnkiClient client, Notes.Note note)
        {
            if (note == null)
                throw new ArgumentNullException(nameof(note));

            var methodInfo = new Func<AnkiClient, Notes.Note, Task<CommandResult<long>>>(AddNoteAsync).Method;
            return await client.Resolve<long>(methodInfo, note);
        }
    }
}