﻿using System;
using System.IO;

namespace AnkiNet
{
    public class AnkiConnectException : Exception
    {
        public Exception Exception { get; set; }
        public string? Request { get; set; }
        public string? Response { get; set; }

        public AnkiConnectException(Exception exception, Stream? request, Stream? response)
        {
            Exception = exception;
            using var requestReader = request == null ? null : new StreamReader(request);
            Request = requestReader?.ReadToEnd();
            using var responseReader = response == null ? null : new StreamReader(response);
            Response = responseReader?.ReadToEnd();
        }
    }
}