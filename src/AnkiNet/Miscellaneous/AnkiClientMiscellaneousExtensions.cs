﻿using System;
using System.Threading.Tasks;

namespace AnkiNet.Miscellaneous
{
    public static class AnkiClientMiscellaneousExtensions
    {
        public static async Task<CommandResult<object>> SyncAsync(this AnkiClient client)
        {
            var methodInfo = new Func<AnkiClient, Task<CommandResult<object>>>(SyncAsync).Method;
            return await client.Resolve<object>(methodInfo);
        }

        public static async Task<CommandResult<object>> ExportPackageAsync(this AnkiClient client, string deck,
            string path, bool includeSched = false)
        {
            var methodInfo =
                new Func<AnkiClient, string, string, bool, Task<CommandResult<object>>>(ExportPackageAsync).Method;
            return await client.Resolve<object>(methodInfo, deck, path, includeSched);
        }

        public static async Task<CommandResult<object>> ImportPackageAsync(this AnkiClient client, string path)
        {
            var methodInfo = new Func<AnkiClient, string, Task<CommandResult<object>>>(ImportPackageAsync).Method;
            return await client.Resolve<object>(methodInfo, path);
        }
    }
}