﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnkiNet.Deck
{
    public static class AnkiClientDeckExtensions
    {
        public static async Task<CommandResult<long>> CreateDeckAsync(this AnkiClient client, string deck)
        {
            var methodInfo = new Func<AnkiClient, string, Task<CommandResult<long>>>(CreateDeckAsync).Method;
            return await client.Resolve<long>(methodInfo, deck);
        }

        public static async Task<CommandResult<object>> DeleteDecksAsync(this AnkiClient client,
            IEnumerable<string> decks, bool cardsToo)
        {
            var methodInfo =
                new Func<AnkiClient, IEnumerable<string>, bool, Task<CommandResult<object>>>(DeleteDecksAsync).Method;
            return await client.Resolve<object>(methodInfo, decks, cardsToo);
        }

        public static async Task<CommandResult<string[]?>> DeckNamesAsync(this AnkiClient client)
        {
            var methodInfo = new Func<AnkiClient, Task<CommandResult<string[]?>>>(DeckNamesAsync).Method;
            return await client.Resolve<string[]?>(methodInfo);
        }
    }
}